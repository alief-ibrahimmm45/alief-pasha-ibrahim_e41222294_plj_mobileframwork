// import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

void main() {
  runApp(const Home());
}

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final List<String> gambar = [
    "bmw.gif",
    "civic.gif",
    "ford.gif",
    "lambo.gif",
    "nsx.gif",
    "pagani.gif",
    "porsche.gif",
    "rx7.gif",
    "supra.gif",
  ];

  static const Map<String, Color> colors = {
    'bmw': Color(0xff2DB569),
    'civic': Color(0xffF386B8),
    'ford': Color(0xff45CAF5),
    'lambo': Color(0xffB19ECB),
    'nsx': Color(0xffF58E4C),
    'pagani': Color(0xff46C1BE),
    'porsche': Color(0xffFFEA0E),
    'rx7': Color(0xffDBE4E9),
  };

  @override
  Widget build(BuildContext context) {
    timeDilation = 5.0;
    return MaterialApp(
      home: Scaffold(
        body: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              begin: FractionalOffset.topCenter,
              colors: [Colors.white, Colors.purple, Colors.deepPurple],
            ),
          ),
          child: PageView.builder(
            controller: PageController(viewportFraction: 0.8),
            itemBuilder: (BuildContext context, int i) {
              return Padding(
                padding: EdgeInsets.symmetric(horizontal: 5.0, vertical: 50.0),
                child: Material(
                  elevation: 8.0,
                  child: Stack(
                    fit: StackFit.expand,
                    children: [
                      Hero(
                        tag: gambar[i],
                        child: Material(
                          child: InkWell(
                            // flex: 1,
                            child: Flexible(
                              child: Container(
                                color: colors.values.elementAt(i),
                                child: Image.asset(
                                  "assets/img/${gambar[i]}",
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => Halamandua(
                                    gambar: gambar[i],
                                    colors: colors.values.elementAt(i),
                                  ),
                                ),
                              );
                            },
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}

class Halamandua extends StatefulWidget {
  Halamandua({required this.gambar, required this.colors});
  final String gambar;
  final Color colors;

  @override
  State<Halamandua> createState() => _HalamanduaState();
}

class _HalamanduaState extends State<Halamandua> {
  Color warna = Colors.grey;

  void _pilihannya(Pilihan pilihan) {
    setState(() {
      warna = pilihan.warna;
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('BT21'),
        backgroundColor: Colors.purpleAccent,
        // ),
        actions: <Widget>[
          new PopupMenuButton<Pilihan>(
            onSelected: _pilihannya,
            itemBuilder: (BuildContext context) {
              return listPilihan.map((Pilihan x) {
                return new PopupMenuItem<Pilihan>(
                  child: new Text(x.teks),
                  value: x,
                );
              }).toList();
            },
          )
        ],
      ),
      body: new Stack(
        children: <Widget>[
          new Container(
            decoration: new BoxDecoration(
              gradient: new RadialGradient(
                  center: Alignment.center,
                  colors: [Colors.purple, Colors.white, Colors.deepPurple]),
            ),
          ),
          new Center(
            child: new Hero(
                tag: widget.gambar,
                child: ClipOval(
                    child: new SizedBox(
                        width: 200.0,
                        height: 200.0,
                        child: new Material(
                          child: new InkWell(
                            onTap: () => Navigator.of(context).pop(),
                            // },
                            child: new Flexible(
                              flex: 1,
                              child: Container(
                                color: widget.colors,
                                child: new Image.asset(
                                  'assets/img/${widget.gambar}',
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                          ),
                        )))),
          )
        ],
      ),
    );
  }
}

class Pilihan {
  const Pilihan({required this.teks, required this.warna});
  final String teks;
  final Color warna;
}

List<Pilihan> listPilihan = const <Pilihan>[
  const Pilihan(teks: "Red", warna: Colors.red),
  const Pilihan(teks: "Green", warna: Colors.green),
  const Pilihan(teks: "Blue", warna: Colors.blue),
];
// class _HomeState extends State<Home> {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//       home: Scaffold(
//         body: Container(
//           decoration: const BoxDecoration(
//             gradient:
//                 LinearGradient(begin: FractionalOffset.topCenter, colors: [
//               Colors.white,
//               Colors.purpleAccent,
//               Colors.deepPurple,
//             ]),
//           ),
//         ),
//       ),
//     );
//   }
// }
