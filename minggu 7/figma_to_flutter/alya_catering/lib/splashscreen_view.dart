import 'dart:html';
import 'package:alya_catering/home_view.dart';
import 'package:flutter/material.dart';
import 'dart:async';

class SplashScreenPage extends StatefulWidget {
  @override
  _SplashScreenPageState createState() => _SplashScreenPageState();
}

class _SplashScreenPageState extends State<SplashScreenPage> {
  @override
  void initState() {
    super.initState();
    // startSplashScreen();
    startTimer();
  }

// startSplashScreen() async {
//   var Duration = const Duration(seconds: 5);
//   return Timer(Duration, () {
//     Navigator.of(context).pushReplacement(
//       MaterialPageRoute(builder: (_) {
//         return Login();
//       }),
//     );
//   ),
// }

  startTimer() async {
    var duratiton = const Duration(seconds: 5);
    return Timer(duratiton, loginRoute);
  }

  loginRoute() {
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => HomePage()));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffFFF1CC),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              'assets/img/alya.png',
              height: 200,
            ),
            const SizedBox(
              height: 20,
            ),
            const CircularProgressIndicator.adaptive(
              valueColor: AlwaysStoppedAnimation<Color>(Color(0xFFFFFFF0)),
            )
          ],
        ),
      ),
    );
  }
}


//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: Stack(
//         children: [
//           Container(
//             decoration: const BoxDecoration(
//               color: Color(0xffFFF1CC),
//             ),
//           ),
//           Center(
//             child: Image.asset('assets/img/alya.png'),
            
//           )
//         ],
//       ),
//     );
//   }
// }
