import 'package:alya_catering/splashscreen_view.dart';
import 'package:flutter/material.dart';
//import 'package:flutter_splashscreen/splashscreen_view.dart';
import 'package:alya_catering/splashscreen_view.dart';

void main(List<String> args) {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: SplashScreenPage(),
    );
  }
}
