class ApiEndpoints {
  static const baseUrl = '192.168.1.6:5000';
  static const prefix = '/api';

  static const signIn = '$prefix/auth';
  static const signUp = '$prefix/auth/signup';

  static const mahasiswa = '$prefix/mahasiswa';
  static String detailMahasiswa(int id) => '$mahasiswa/$id';
}
