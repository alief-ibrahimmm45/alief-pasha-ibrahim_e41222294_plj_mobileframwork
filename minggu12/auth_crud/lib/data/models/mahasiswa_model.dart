class MahasiswaModel {
  MahasiswaModel({
    required this.id,
    required this.name,
    required this.nim,
    required this.angkatan,
    required this.jurusan,
    required this.createdAt,
  });

  int id;
  String name;
  String nim;
  String angkatan;
  String jurusan;
  DateTime createdAt;

  factory MahasiswaModel.fromJson(Map<String, dynamic> json) => MahasiswaModel(
        id: json["id"],
        name: json["name"],
        nim: json["nim"],
        angkatan: json["angkatan"],
        jurusan: json["jurusan"],
        createdAt: DateTime.parse(json["created_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "nim": nim,
        "angkatan": angkatan,
        "jurusan": jurusan,
        "created_at": createdAt.toIso8601String(),
      };
}
