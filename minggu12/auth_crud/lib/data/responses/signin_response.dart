import 'dart:convert';

import 'package:auth_crud/data/models/user_model.dart';

SignInResponse signInResponseFromJson(String str) =>
    SignInResponse.fromJson(json.decode(str));

String signInResponseToJson(SignInResponse data) => json.encode(data.toJson());

class SignInResponse {
  SignInResponse({
    required this.success,
    required this.message,
    required this.data,
    required this.token,
  });

  bool success;
  String message;
  UserModel data;
  String token;

  factory SignInResponse.fromJson(Map<String, dynamic> json) => SignInResponse(
        success: json["success"],
        message: json["message"],
        data: UserModel.fromJson(json["data"]),
        token: json["token"],
      );

  Map<String, dynamic> toJson() => {
        "success": success,
        "message": message,
        "data": data.toJson(),
        "token": token,
      };
}
