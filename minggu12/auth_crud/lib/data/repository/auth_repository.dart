import 'dart:convert';
import 'dart:developer';
import 'package:auth_crud/data/remote/api_endpoints.dart';
import 'package:auth_crud/data/responses/signin_response.dart';
import 'package:auth_crud/data/responses/signup_response.dart';
import 'package:http/http.dart' as http;

class AuthRepository {
  static const tag = 'AUTH_REPOSITORY';

  static Future<SignInResponse> signIn({
    required String username,
    required String password,
  }) async {
    try {
      final uri = Uri.http(ApiEndpoints.baseUrl, ApiEndpoints.signIn);
      final headers = {'Accept': 'application/json'};
      final params = {'username': username, 'password': password};

      log('URI: ${uri.toString()}');
      log('headers: ${headers.toString()}');
      log('params: ${params.toString()}');

      final response = await http.post(uri, headers: headers, body: params);
      final responseBody = jsonDecode(response.body);

      if (response.statusCode == 200) {
        return SignInResponse.fromJson(responseBody);
      }

      throw responseBody['message'];
    } catch (e) {
      log(e.toString(), name: tag);
      rethrow;
    }
  }

  static Future<SignupResponse> signUp({
    required String name,
    required String username,
    required String email,
    required String password,
  }) async {
    try {
      final uri = Uri.http(ApiEndpoints.baseUrl, ApiEndpoints.signUp);
      final headers = {'Accept': 'application/json'};
      final params = {
        'name': name,
        'username': username,
        'email': email,
        'password': password,
      };

      log('URI: ${uri.toString()}');
      log('headers: ${headers.toString()}');
      log('params: ${params.toString()}');

      final response = await http.post(uri, headers: headers, body: params);
      final responseBody = jsonDecode(response.body);

      if (response.statusCode == 201) {
        return SignupResponse.fromJson(responseBody);
      }

      throw responseBody['message'];
    } catch (e) {
      log(e.toString(), name: tag);
      rethrow;
    }
  }
}
