import 'package:auth_crud/data/models/mahasiswa_model.dart';
import 'package:auth_crud/data/repository/mahasiswa_repository.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'home_screen.dart';

class EditScreen extends StatefulWidget {
  const EditScreen({Key? key}) : super(key: key);

  @override
  State<EditScreen> createState() => _EditScreenState();
}

class _EditScreenState extends State<EditScreen> {
  final formKey = GlobalKey<FormState>();
  final inputName = TextEditingController();
  final inputNim = TextEditingController();
  final inputAngkatan = TextEditingController();
  final inputJurusan = TextEditingController();

  final mahasiswa = Get.arguments['mahasiswa'] as MahasiswaModel;

  @override
  void initState() {
    super.initState();

    inputName.text = mahasiswa.name;
    inputNim.text = mahasiswa.nim;
    inputAngkatan.text = mahasiswa.angkatan;
    inputJurusan.text = mahasiswa.jurusan;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Update Mahasiswa')),
      body: Form(
        key: formKey,
        child: SingleChildScrollView(
          padding: const EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              TextFormField(
                controller: inputName,
                decoration: const InputDecoration(hintText: 'Name'),
                validator: (value) {
                  if (value.toString().isEmpty) {
                    return 'Please provide name';
                  }

                  return null;
                },
              ),
              const SizedBox(height: 16),
              TextFormField(
                controller: inputNim,
                decoration: const InputDecoration(hintText: 'NIM'),
                validator: (value) {
                  if (value.toString().isEmpty) {
                    return 'Please provide NIM';
                  }

                  return null;
                },
              ),
              const SizedBox(height: 16),
              TextFormField(
                controller: inputAngkatan,
                decoration: const InputDecoration(hintText: 'Angkatan'),
                validator: (value) {
                  final angkatan = value.toString();
                  if (angkatan.isEmpty) {
                    return 'Please provide angkatan';
                  }

                  return null;
                },
              ),
              const SizedBox(height: 16),
              TextFormField(
                controller: inputJurusan,
                decoration: const InputDecoration(hintText: 'Jurusan'),
                validator: (value) {
                  if (value.toString().isEmpty) {
                    return 'Please provide jurusan';
                  }

                  return null;
                },
              ),
              const SizedBox(height: 24),
              ElevatedButton(
                onPressed: _updateMahasiswa,
                child: const Text('UPDATE'),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _updateMahasiswa() async {
    if (!formKey.currentState!.validate()) return;

    try {
      await MahasiswaRepository.updateMahasiswa(
        id: mahasiswa.id,
        name: inputName.text.trim(),
        nim: inputNim.text.trim(),
        angkatan: inputAngkatan.text.trim(),
        jurusan: inputJurusan.text.trim(),
      );

      Fluttertoast.showToast(msg: 'Update Mahasiswa Success');
      Get.until((route) => route.isFirst);
    } catch (e) {
      Fluttertoast.showToast(msg: e.toString());
    }
  }

  // void deleteData() {
  //   var url = '';
  //   final uri = uri.parse(url + widget.list[widget.index]['id'].toString());
  //   log(uri.toString());
  //   http.delete(uri);
  // }
}
