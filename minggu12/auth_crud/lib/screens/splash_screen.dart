import 'package:auth_crud/data/local/session.dart';
import 'package:auth_crud/routes/route_names.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'dart:async';
import 'dart:ui';
import 'package:flutter/gestures.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({Key? key}) : super(key: key);

  _navigateNext() async {
    await Future.delayed(const Duration(seconds: 3));
    final isSignedIn = await Session.checkIsSignedIn();
    if (isSignedIn) {
      return Get.offNamed(RouteNames.homeScreen);
    }

    Get.offNamed(RouteNames.signinScreen);
  }

  @override
  Widget build(BuildContext context) {
    _navigateNext();

    return Scaffold(
      body: Stack(children: [
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/splash.png"),
              fit: BoxFit.cover,
            ),
          ),
        ),
        // Column(
        //   crossAxisAlignment: CrossAxisAlignment.stretch,
        //   mainAxisAlignment: MainAxisAlignment.start,
        //   children: [
        //     const SizedBox(height: 200),
        //     BackdropFilter(
        //       filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
        //       child: Padding(
        //         padding: const EdgeInsets.symmetric(horizontal: 16),
        //         child: Text(
        //           "",
        //           textAlign: TextAlign.center,
        //           style: TextStyle(
        //             fontSize: 30,
        //             fontWeight: FontWeight.w700,
        //             color: Color.fromARGB(255, 86, 22, 97),
        //           ),
        //         ),
        //       ),
        //     ),
        //   ],
        // )
      ]),
    );
  }
}
