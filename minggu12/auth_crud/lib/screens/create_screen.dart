import 'package:auth_crud/data/repository/mahasiswa_repository.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';

class CreateScreen extends StatefulWidget {
  const CreateScreen({Key? key}) : super(key: key);

  @override
  State<CreateScreen> createState() => _CreateScreenState();
}

class _CreateScreenState extends State<CreateScreen> {
  final formKey = GlobalKey<FormState>();
  final inputName = TextEditingController();
  final inputNim = TextEditingController();
  final inputAngkatan = TextEditingController();
  final inputJurusan = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Create Mahasiswa')),
      body: Form(
        key: formKey,
        child: SingleChildScrollView(
          padding: const EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              TextFormField(
                controller: inputName,
                decoration: const InputDecoration(hintText: 'Name'),
                validator: (value) {
                  if (value.toString().isEmpty) {
                    return 'Please provide name';
                  }

                  return null;
                },
              ),
              const SizedBox(height: 16),
              TextFormField(
                controller: inputNim,
                decoration: const InputDecoration(hintText: 'NIM'),
                validator: (value) {
                  if (value.toString().isEmpty) {
                    return 'Please provide NIM';
                  }

                  return null;
                },
              ),
              const SizedBox(height: 16),
              TextFormField(
                controller: inputAngkatan,
                decoration: const InputDecoration(hintText: 'Angkatan'),
                validator: (value) {
                  final angkatan = value.toString();
                  if (angkatan.isEmpty) {
                    return 'Please provide angkatan';
                  }

                  return null;
                },
              ),
              const SizedBox(height: 16),
              TextFormField(
                controller: inputJurusan,
                decoration: const InputDecoration(hintText: 'Jurusan'),
                validator: (value) {
                  if (value.toString().isEmpty) {
                    return 'Please provide jurusan';
                  }

                  return null;
                },
              ),
              const SizedBox(height: 24),
              ElevatedButton(
                onPressed: _createMahasiswa,
                child: const Text('CREATE'),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _createMahasiswa() async {
    if (!formKey.currentState!.validate()) return;

    try {
      await MahasiswaRepository.createMahasiswa(
        name: inputName.text.trim(),
        nim: inputNim.text.trim(),
        angkatan: inputAngkatan.text.trim(),
        jurusan: inputJurusan.text.trim(),
      );

      Fluttertoast.showToast(msg: 'Create Mahasiswa Success');
      Get.until((route) => route.isFirst);
    } catch (e) {
      Fluttertoast.showToast(msg: e.toString());
    }
  }
}
