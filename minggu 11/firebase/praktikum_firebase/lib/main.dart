import 'dart:html';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:praktikum_firebase/LoginScreen.dart';

Future<void> void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.inittializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // @Override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'flutter demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: LoginScreen(),
    );
  }
}