-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 16, 2022 at 04:11 PM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.0.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `data_karyawan`
--

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jeniskelamin` enum('Laki-laki','Perempuan') COLLATE utf8mb4_unicode_ci NOT NULL,
  `notelpon` bigint(20) NOT NULL,
  `foto` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `nama`, `jeniskelamin`, `notelpon`, `foto`, `created_at`, `updated_at`) VALUES
(25, 'riya ibrahim', 'Laki-laki', 82334878291, '1.png', '2022-11-10 00:21:36', '2022-11-10 00:27:21'),
(26, 'erina ibrahim', 'Laki-laki', 876466464, '5.jpg', '2022-11-10 00:28:20', '2022-11-10 00:28:20'),
(27, 'asd', 'Laki-laki', 82334878291, '1.png', '2022-11-11 00:46:22', '2022-11-11 00:46:22'),
(28, 'asld', 'Laki-laki', 82334878291, '4.jpg', '2022-11-11 00:47:08', '2022-11-11 00:47:08'),
(29, 'yhrfb', 'Perempuan', 82334878291, '6.png', '2022-11-11 00:47:22', '2022-11-11 00:47:22'),
(30, 'tggtg', 'Laki-laki', 82334878291, '4.jpg', '2022-11-11 00:47:38', '2022-11-11 00:47:38'),
(35, 'andre', 'Laki-laki', 82334878291, '2.png', '2022-11-16 07:16:54', '2022-11-16 07:16:55'),
(36, 'vertiii', 'Perempuan', 82334878291, '5.jpg', '2022-11-16 07:17:16', '2022-11-16 07:17:16'),
(37, 'gerger', 'Laki-laki', 82334878291, '2.png', '2022-11-16 07:17:39', '2022-11-16 07:17:39'),
(38, 'bangkosmhgdsjkk', 'Laki-laki', 84390304903, '4.jpg', '2022-11-16 07:17:57', '2022-11-16 07:17:57'),
(39, 'nesukosmi isksldjs sols', 'Laki-laki', 86375628673, '6.png', '2022-11-16 07:18:28', '2022-11-16 07:18:28');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
