<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\backend\EmployeeController;
use App\Http\Controllers\backend\ApiPegawaiController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
// Route::group(['namespace' => 'backend'], function () {
//     Route::get('produk', 'ApiProdukController@getAll');
//     Route::get('produk/{id}', 'ApiProdukController@getPen');
//     Route::post('produk', 'ApiProdukController@createPen');
//     Route::put('produk/{id}', 'ApiProdukController@updatePen');
//     Route::delete('produk/{id}', 'ApiProdukController@deletePen');
        
//     });

Route::controller(ApiPegawaiController::class)->group(function (){
    Route::get('employee', 'getAll');
    Route::get('employee/{id}', 'getPen');
    Route::post('employee', 'createPen');
    Route::put('employee/{id}', 'updatePen');
    Route::delete('employee/{id}', 'deletePen');
});