@extends('layout.admin')
@section('content')



<div class="content-wrapper">
  <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0">Data Pegawai</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active">Dashboard v2</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <div class="container">
        <div class="row">
        @if ($message = Session::get('success'))
        <div class="alert alert-success" role="alert">
          {{ $message }}
        </div>
        @endif
        </div>
                  <table class="table table-striped">
                        <thead>
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nama</th>
                            <th scope="col">Foto</th>
                            <th scope="col">Jenis Kelamin</th>
                            <th scope="col">No Telepon</th>
                            <th scope="col">Dibuat</th>
                            <th scope="col">Aksi</th>
                          </tr>
                        </thead>
                        <a href="/tambahpegawai"><button type="button" class="btn btn-success">CREATE +</button></a>
                        <div class="row g-3 align-items-center mt-2">
                          <div class="col-auto">
                            <form action="/pegawai" method="GET">
                          </div>
                        </form>
                          <div class="col-auto">
                            <input type="search" id="inputPassword6" name="search" class="form-control">
                          </div>
                          <div class="col-auto">
                            <a href="exportpdf" class="btn btn-danger">Export PDF</button></a>
                              <button type="button" class="btn btn-info" data-bs-toggle="modal" data-bs-target="#exampleModal">
                                Import Data
                              </button>
                          </div>
                        </div>
                <!-- Button trigger modal -->
                        <!-- Modal -->
                        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                          <div class="modal-dialog">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h1 class="modal-title fs-5" id="exampleModalLabel">Modal title</h1>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                              </div>
                              <form action="#" method="POST" enctype="multipart/form-data">
                              @csrf
                              <div class="modal-body">
                              <div class="form-group">
                                <input type="file" name="file" required>
      
                              </div>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary">Submit</button>
                              </div>
                            </div>
                          </form>
                          </div>
                        </div>
      
                        <tbody>
                          @php
                            $no = 1;  
                          @endphp
      
                          @foreach ($data as $index => $row)
                          <tr>
                            <th scope="row">{{ $index + $data->firstitem() }}</th>
                            <td>{{ $row->nama }}</td>
                            <td>
                                <img src="{{ asset('fotopegawai/'.$row->foto) }}"alt=""style="width: 60px;">
                            </td>
                            <td>{{ $row->jeniskelamin}}</td>
                            <td>0{{ $row->notelpon }}</td>
                            <td>{{ $row->created_at->format('D M Y') }}</td>
                            <td>
                                <a href="/tampilkandata/{{ $row->id }}" class="btn btn-primary">Edit</a>
                                <a href="/delete/{{ $row->id }}" class="btn btn-danger">delete</a>
                            </td>
                          </tr>
                          @endforeach
                                </tbody>
                                <body>
                              </table>
                              {{ $data->links() }}
                              <script>
                                $('.delete').click( function(){
                                  var Pegawaiid = $(this).attr('data-id');
                                  swal({
                                          title: "Kamu Yakin?",
                                          text: "Kamu Akan Menghapus Data Pegawai Ini dengan id "+Pegawaiid+" ",
                                          icon: "warning",
                                          buttons: true,
                                          dangerMode: true,
                                        })
                                        .then((willDelete) => {
                                          if (willDelete) {
                                            window.location = "/delete/"+Pegawaiid+" ",
                                            swal("Data Berhasil Dihapus!", {
                                              icon: "Berhasil Menghapus Data",
                                            });
                                          } else {
                                            swal("Data Tidak Jadi Di Hapus");
                                          }
                                      });
                                      });
                                    </script>
                                  </div>
                              </body>
                            </div>
                          @endsection



















                          

                          @push('name')
  
                          <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>
                          <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
                          <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css" integrity="sha512-3pIirOrwegjM6erE5gPSwkUzO+3cTjpnV9lexlNZqvupR64iZBnOOTiiLPb9M36zpMScbmUNIcHUqKD47M719g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
                          <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
                          <script src="https://code.jquery.com/jquery-3.6.1.min.js" integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script>
                          <script src="https://code.jquery.com/jquery-3.6.1.slim.js" integrity="sha256-tXm+sa1uzsbFnbXt8GJqsgi2Tw+m4BLGDof6eUPjbtk=" crossorigin="anonymous"></script> 
                          <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
                          <script src="https://code.jquery.com/jquery-3.6.1.slim.min.js" integrity="sha256-w8CvhFs7iHNVUtnSP0YKEg00p9Ih13rlL9zGqvLdePA=" crossorigin="anonymous"></script>
                          @endpush
                          <script src="https://code.jquery.com/jquery-3.6.1.slim.min.js" integrity="sha256-w8CvhFs7iHNVUtnSP0YKEg00p9Ih13rlL9zGqvLdePA=" crossorigin="anonymous"></script>



















