<?php

namespace App\Http\Controllers\backend;

use App\Models\employee;
use Illuminate\Http\Request,
    App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;


class ApiProdukController extends Controller
{
    //
    public function getAll()
    {
        $data = employee::all();
        return Response::json($data, 201);
    }

public function getPen($id)
{
    $data = employee::find($id);
    return Response::json($data, 200);
}

public function createPen(Request $request)
{
    employee::create($request->all());
    return response()->json([
        'status' => 'ok',
        'message' => 'Produk berhasil ditambahkan!'
    ], 201);
}

public function updatePen($id, Request $request)
{
    employee::find($id)->update($request->all());
    return response()->json([
        'status' => 'ok',
        'message' => 'Produk berhasil dirubah!'
    ], 201);
}

public function deletePen($id)
{
    employee::destroy($id);
    return response()->json([
        'status' => 'ok',
        'message' => 'Produk berhasil dihapus!'
    ], 201);
}
}