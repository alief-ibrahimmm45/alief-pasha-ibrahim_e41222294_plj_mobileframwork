<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('Employees')->insert([
        'nama' => 'Alief Pasha Ibrahim',
        'jeniskelamin' => 'Laki-Laki',
        'notelpon' => '082334878291',
      ]); //
    }
}
